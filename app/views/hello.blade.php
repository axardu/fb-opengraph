<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>

</head>
<body>
	<div class="welcome">
		<h1>You have arrived.</h1>
		<h2>Login with facebook</h2>

		@if(Session::has('message'))
    {{ Session::get('message')}}
		@endif
		<br>
		@if (!empty($data))
		    Hello, {{{ $data['name'] }}} 
		    <img src="{{ $data['photo']}}">
		    <br>
		    Your email is {{ $data['email']}}
		    <br>
		    <a href="logout">Logout</a>
		@else
		    Hi! Would you like to <a href="login/fb">Login with Facebook</a>?
		@endif
			</div>
</body>
</html>
